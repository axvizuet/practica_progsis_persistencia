#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "medicion.h"

float ftoc(float f){

	return(f - 32)*(5.0f/9.0f);

}

int main()
{
	FILE *fp;
	char filename[MAXLINE];
	Medicion_t m;

	printf("Ingrese nommbre archivo: ");
	scanf("%s",filename);

	fp = fopen(filename,"r");

	if(fp==NULL){
                fprintf(stderr, "No se pudo abrir archivo\n");
                return 1;
        }
        //Salta la primera linea (cabecera del archivo)
        fscanf(fp,"%*[^\n]\n");
	
	char celsius_name[] = "celsius_";
	char new_file[strlen(filename)+strlen(celsius_name)];
	strcpy(new_file,celsius_name);
	strcat(new_file,filename);

	FILE *fout = fopen(new_file,"w");
	if(fout==NULL){
		fprintf(stderr, "No se pudo crear archivo\n" );
		fclose(fp);
		return 1;
	}
	
	int n=0;
	do{

		n = fscanf(fp, "%s\t%s\t%f\t%f\t%d\t%f\t%f", m.date, m.time, &m.temperature, &m.humidity, &m.pressure, &m.altitude, &m.battery);
		if(n >= 7){
			fprintf(fout,"%s\t%s\t%f\t%f\t%d\t%f\t%f\n", m.date, m.time, ftoc(m.temperature), m.humidity, m.pressure, m.altitude, m.battery);
		}else if(n != EOF){
			fprintf(stderr, "No se pudo leer el archivo\n");
			fclose(fp);
			fclose(fout);
			return 1;
		}
	}while(n>=7);

	fclose(fp);
	fclose(fout);
	return 0;
}
